<?php

namespace xtetis\xseo;

/**
 * Компонент для работы с SEO
 */
class Component extends \xtetis\xengine\models\Component
{
    /**
     * Дополнительный обработчик SEF урлов
     */
    public static function additionalSefHanler($url = '')
    {
        $url = strval($url);
        $model_seo_page = \xtetis\xseo\models\SeoPageModel::getSeoModelByUrl($url);

        // Если для указанного урла есть настройки SEO страницы
        if ($model_seo_page)
        {
            // Если это SEF УРЛ
            if ($model_seo_page->use_sef_params)
            {
                \xtetis\xengine\App::getApp()->component_name = $model_seo_page->sef_route_component;
                \xtetis\xengine\App::getApp()->action_name    = $model_seo_page->sef_route_action;
                \xtetis\xengine\App::getApp()->query_name     = $model_seo_page->sef_route_query;

                $_GET = $_GET + $model_seo_page->getSefRouteGetArray();

                return true;
            }

            // Если нужен редирект
            if ($model_seo_page->use_redirect)
            {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$model_seo_page->redirect_to);
                exit();
            }
        }


        return false;
    }


    /**
     * Если установлены в компоненте TITLE | DESCRIPTION
     * Делаем для страницы именно их
     */
    public static function processTdz()
    {
        $url = $_SERVER['REQUEST_URI'];

        $model_seo_page = \xtetis\xseo\models\SeoPageModel::getSeoModelByUrl($url);

        // Если для указанного урла есть настройки SEO страницы
        if ($model_seo_page)
        {
            // Если заполнен title
            if (strlen($model_seo_page->title))
            {
                \xtetis\xengine\helpers\SeoHelper::setTitle($model_seo_page->title,true);
            }

            // Если заполнен description
            if (strlen($model_seo_page->description))
            {
                \xtetis\xengine\helpers\SeoHelper::setDescription($model_seo_page->description,true);
            }
        }


        return  true;
    }

}
