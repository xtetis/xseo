<?php

namespace xtetis\xseo\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 * Модель для работы с сеотекстами
 */
class SeoLinkModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xseo_link';

    /**
     * Ссылка на продвигаемую страницу
     */
    public $id_seo_page = null;

    /**
     * Анкор ссылки
     * Также может быть текстом. Для вставки ссылки в конкретное место необходимо использовать
     * знак "#", например "текст текст #анкор# текст текст"
     */
    public $anchor = '';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_seo_page' => \xtetis\xseo\models\SeoPageModel::class,
    ];

    /**
     * Добавляет SeoLink
     */
    public function addSeoLink()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_seo_page = intval($this->id_seo_page);
        $this->anchor      = strval($this->anchor);

        if (!strlen($this->anchor))
        {
            $this->addError('anchor', 'Не указано anchor');

            return false;
        }

        $model = \xtetis\xseo\models\SeoPageModel::generateModelById($this->id_seo_page);
        if (!$model)
        {
            $this->addError('id_seo_page', 'Страница не существует');

            return false;
        }

        $this->insert_update_field_list = [
            'id_seo_page',
            'anchor',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует SeoLink
     */
    public function editSeoLink()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Запись не существует');

            return false;
        }

        $this->id_seo_page = intval($this->id_seo_page);
        $this->anchor      = strval($this->anchor);

        if (!strlen($this->anchor))
        {
            $this->addError('anchor', 'Не указано anchor');

            return false;
        }

        $model = \xtetis\xseo\models\SeoPageModel::generateModelById($this->id_seo_page);
        if (!$model)
        {
            $this->addError('id_seo_page', 'Страница не существует');

            return false;
        }

        $this->insert_update_field_list = [
            'id_seo_page',
            'anchor',
        ];

        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Удаляем SeoLink
     */
    public function deleteSeo()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Запись не существует');

            return false;
        }

        $this->deleteTableRecordById();
    

        return true;
    }

    /**
     * Возвращает массив случайных ссылок на продвигаемые страницы
     */
    public static function getRandomLinks($limit = 5)
    {
        $limit      = intval($limit);
        $cache_id   = 'random_links_page_' . md5($_SERVER['REQUEST_URI']) . '_' . $limit;
        $cache      = \xtetis\xengine\libraries\Cache::getCacheObj();
        $cache_data = $cache->get($cache_id);
        $links      = [];
        if (!$cache_data)
        {

            $self_model = new self();

            $sql = '
            SELECT
                `id`
            FROM
            (
                SELECT `id`, `id_seo_page`, `anchor`
                FROM `' . $self_model->table_name . '`
                WHERE
                    id_seo_page IN
                    (
                        SELECT DISTINCT `id_seo_page`
                        FROM `' . $self_model->table_name . '` l
                        WHERE l.id_seo_page IS NOT NULL
                        ORDER BY RAND()

                    )
                    AND
                    id_seo_page IS NOT NULL
                ORDER BY RAND()
            ) tmp
            GROUP BY tmp.id_seo_page
            ORDER BY RAND()
            LIMIT
            ' . $limit;

            // Получаем список моделей SEO Link
            $model_list = self::getModelListBySql($sql);
            foreach ($model_list as $model_seo_link)
            {
                $model_seo_page = $model_seo_link->getModelRelated('id_seo_page');
                if ($model_seo_page)
                {
                    $links[$model_seo_page->url] = $model_seo_link->anchor;

                }
            }

            $cache->set($cache_id, $links);
        }
        else
        {
            $links = $cache_data;
        }

        $links = self::prepareLinks($links);

        return $links;
    }

    public static function prepareLinks($links = [])
    {
        foreach ($links as $url => &$anchor) {
            $anchor_array = explode('#',$anchor);
            if (count($anchor_array) == 3)
            {
                $anchor = $anchor_array[0].
                '<a href="'.htmlspecialchars($url, ENT_QUOTES).'">'.
                $anchor_array[1].
                '</a>'.
                $anchor_array[2];
            }
            else
            {
                $anchor = '<a href="'.htmlspecialchars($url, ENT_QUOTES).'">'.
                $anchor.'</a>';
            }
        }
        return $links;
    }
}


