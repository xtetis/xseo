<?php

namespace xtetis\xseo\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

global $seo_page;

/**
 * Модель для работы с сеотекстами
 */
class SeoPageModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xseo_page';

    /**
     * Урл странинцы
     */
    public $url = '';

    /**
     * Сеотекст
     */
    public $seotext = '';

    /**
     *     Title страницы
     */
    public $title = '';

    /**
     * Description страницы
     */
    public $description = '';

    /**
     * Активность записи
     */
    public $active = 0;

    /**
     * Тег H1
     */
    public $hone = '';

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Имя страницы (используется для админки)
     */
    public $name = '';

    /**
     * Использовать SEF параметры
     */
    public $use_sef_params = 0;

    /**
     * Загружаемый SEF Route component
     */
    public $sef_route_component = '';

    /**
     * Загружаемый SEF Route action
     */
    public $sef_route_action = '';

    /**
     * Загружаемый SEF Route query
     */
    public $sef_route_query = '';

    /**
     * Загружаемые Route GET параметры
     */
    public $sef_route_get = null;

    /**
     * Использовать редирект
     */
    public $use_redirect = 0;

    /**
     * Редиректить на какую страницу
     */
    public $redirect_to = '';

    /**
     * Добавляет SEO
     */
    public function addSeo()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->url         = strval($this->url);
        $this->title       = strval($this->title);
        $this->description = strval($this->description);
        $this->hone        = strval($this->hone);
        $this->name        = strval($this->name);
        $this->seotext     = strval($this->seotext);

        $this->use_sef_params      = intval($this->use_sef_params);
        $this->sef_route_component = strval($this->sef_route_component);
        $this->sef_route_action    = strval($this->sef_route_action);
        $this->sef_route_query     = strval($this->sef_route_query);
        $this->sef_route_get       = strval($this->sef_route_get);

        $this->use_redirect = intval($this->use_redirect);
        $this->redirect_to  = strval($this->redirect_to);

        if (!strlen($this->url))
        {
            $this->addError('url', 'Не указано url');

            return false;
        }

        $this->insert_update_field_list = [
            'url',
            'title',
            'description',
            'hone',
            'name',
            'seotext',
            'use_sef_params',
            'sef_route_component',
            'sef_route_action',
            'sef_route_query',
            'sef_route_get',
            'use_redirect',
            'redirect_to',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует
     */
    public function editSeo()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Запись не существует');

            return false;
        }

        $this->url         = strval($this->url);
        $this->title       = strval($this->title);
        $this->description = strval($this->description);
        $this->hone        = strval($this->hone);
        $this->name        = strval($this->name);
        $this->seotext     = strval($this->seotext);

        $this->use_sef_params      = intval($this->use_sef_params);
        $this->sef_route_component = strval($this->sef_route_component);
        $this->sef_route_action    = strval($this->sef_route_action);
        $this->sef_route_query     = strval($this->sef_route_query);
        $this->sef_route_get       = strval($this->sef_route_get);

        $this->use_redirect = intval($this->use_redirect);
        $this->redirect_to  = strval($this->redirect_to);

        if (!strlen($this->url))
        {
            $this->addError('url', 'Не указано url');

            return false;
        }

        $this->insert_update_field_list = [
            'url',
            'title',
            'description',
            'hone',
            'name',
            'seotext',
            'use_sef_params',
            'sef_route_component',
            'sef_route_action',
            'sef_route_query',
            'sef_route_get',
            'use_redirect',
            'redirect_to',
        ];

        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Удаляем
     */
    public function deleteSeo()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Запись не существует');

            return false;
        }

        if (!$this->deleteTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Изменяет активацию
     */
    public function setActive($active = 0)
    {

        if ($this->getErrors())
        {
            return false;
        }

        $active = intval($active);
        if ($active)
        {
            $active = 1;
        }

        $this->active                   = $active;
        $this->insert_update_field_list = ['active'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;

    }

    /**
     * Возвращает настройки SEO для текущей страницы
     */
    public static function getSeoModelByUrl($url = '')
    {
        global $seo_page;

        if (!strlen($url))
        {
            $url = $_SERVER['REQUEST_URI'];
        }

        $model = new self();

        if (!isset($seo_page))
        {

            $sql = 'SELECT id FROM ' . $model->table_name . ' WHERE url = :url';

            // Получаем список моделей
            $model_list = self::getModelListBySql(
                $sql, 
                ['url' => $url],
                [
                    'cache'=>true,
                    'cache_tags'=>[
                        $model->table_name
                    ]
                ]
            );

            if ($model_list)
            {
                foreach ($model_list as $model_item)
                {
                    $seo_page = $model_item;

                    return $seo_page;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return $seo_page;
        }
    }

    /**
     * Возвращает модели SEO Link для текущей страницы
     */
    public function getSeoLinkModels()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $sql = '
        SELECT id FROM `xseo_link` WHERE `id_seo_page` =
        ' . $this->id;

        // Получаем список моделей SEO Link

        return \xtetis\xseo\models\SeoLinkModel::getModelListBySql($sql);
    }

    /**
     * Возвращает сеотекст
     */
    public function getSeotext()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return $this->seotext;
    }

    /**
     * Возвращает список страниц как опции для SELECT
     */
    public function getOptionsParent($add_empty = false)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = 'SELECT id FROM ' . $this->table_name . '';

        $models = self::getModelListBySql(
            $sql
        );

        $options = [];

        if ($add_empty)
        {
            $options[0] = 'Корень';
        }

        foreach ($models as $id => $model)
        {
            $options[$id] = $model->url . ' (' . $model->name . ')';
        }

        return $options;
    }

    /**
     * Возвращает параметры GET для SEF страницы
     */
    public function getSefRouteGetArray()
    {
        $ret = [];
        parse_str($this->sef_route_get, $ret);

        return $ret;

    }
}
